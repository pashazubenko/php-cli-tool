# php-cli-tool

Usage: 

    php script.php [--help] [-cp|--contracts-path] [-pp|--partner-path] [-p|--partner] [-d|--date]

Options:

*    --help             Show this help message 
*    -cp  --contracts-path   Path to file with Music Contracts data.
*    -pp  --partner-path     Path to file with Distribution Partner Contracts data.
*    -p   --partner          Filter by Partner name.
*    -d   --date             Filter by Date.

Example:

    php script.php --contracts-path=contracts.txt --partner-path=distribution_partner.txt --partner=YouTube --date='27st Dec 2012'