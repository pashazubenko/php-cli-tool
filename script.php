#!/usr/bin/env php

<?php
// --------------------
//  Data Input
// --------------------

// Sert Params validation options
$params = [
    ''      => 'help',
    'cp:'  => 'contracts-path:',
    'pp:'  => 'partner-path:',
    'p::'    => 'partner::',
    'd::'    => 'date::',
];

// Default values
$contracts_path  = null;
$partner_path    = null;
$partner         = null;
$date            = null;
$errors     = [];

// Parse options from cli
$options = getopt(implode('', array_keys($params)), $params);

// Validate options
if (isset($options['contracts-path']) || isset($options['cp'])) {
    $contracts_path = isset( $options['contracts-path'] ) ? $options['contracts-path'] : $options['cp'];
} else {
    $errors[] = 'contracts-path required';
}

if (isset($options['partner-path']) || isset($options['pp'])) {
    $partner_path = isset( $options['partner-path'] ) ? $options['partner-path'] : $options['pp'];
} else {
    $errors[] = 'partner-path required';
}

if (isset($options['partner']) || isset($options['p'])) {
    $partner = isset( $options['partner'] ) ? $options['partner'] : $options['p'];
} 

if (isset($options['date']) || isset($options['d'])) {
    $date = isset( $options['date'] ) ? $options['date'] : $options['d'];
} 

// Help output & Input Error listing
if ( isset($options['help']) || count($errors) ) {
    $help = "
usage: php script.php [--help] [-cp|--contracts-path] [-pp|--partner-path] [-p|--partner] [-d|--date]

Options:
             --help             Show this help message 
        -cp  --contracts-path   Path to file with Music Contracts data.
        -pp  --partner-path     Path to file with Distribution Partner Contracts data.
        -p   --partner          Filter by Partner name.
        -d   --date             Filter by Date.
Example:
        php script.php --contracts-path=contracts.txt --partner-path=distribution_partner.txt --partner=YouTube --date='27st Dec 2012'
";
    if ( $errors ) {
        $help .= 'Errors:' . PHP_EOL . implode("\n  ", $errors) . PHP_EOL;
    }
    die($help);
}

// --------------------
//  Data Parse
// --------------------

// Partner data parse
$partner_data      = [];
$partner_file      = file_get_contents($partner_path);
$partner_file_rows = explode("\n", $partner_file);

array_shift($partner_file_rows); // slise table titles

foreach($partner_file_rows as $row => $data) {
    $row_data = explode('|', $data);
    $partner_data[$row_data[0]] = $row_data[1];
}

// Date parse
$date_info = date_parse($date);
if ($date_info['warning_count']) {
    die(PHP_EOL . 'Date format not valid!' . PHP_EOL);
}

// Contracts data parse
$contracts_file      = file_get_contents($contracts_path);
$contracts_file_rows = explode("\n", $contracts_file);

// Slise and Print table headers
$headers = array_shift($contracts_file_rows); 
echo PHP_EOL . $headers . PHP_EOL;


// --------------------
//  Print results
// --------------------
foreach($contracts_file_rows as $row => $data) {
    $row_data   = explode('|', $data);
    $row_usages = explode(', ', $row_data[2]);

    $start_day  = date_parse($row_data[3]);
    $end_day    = date_parse($row_data[4]);
    
    $index = array_search($partner_data[$partner], $row_usages);

    if ($index !== false) { 

        $date_diff = mktime (0, 0, 0, $start_day['month'], $start_day['day'], $start_day['year']) - mktime (0, 0, 0, $date_info['month'], $date_info['day'], $date_info['year']);
        
        if ($date_diff <= 0) {
            if ($end_day['error_count'] < 0) {
                $date_diff = mktime (0, 0, 0, $end_day['month'], $end_day['day'], $end_day['year']) - mktime (0, 0, 0, $date_info['month'], $date_info['day'], $date_info['year']);
                
                
                if ($date_diff > 0) {
                    echo $row_data[0] . '|' . $row_data[1] . '|' . $partner_data[$partner] . '|' . $row_data[3] . '|' . $row_data[4] . PHP_EOL;
                }
            } 
            else {
                    echo $row_data[0] . '|' . $row_data[1] . '|' . $partner_data[$partner] . '|' . $row_data[3] . '|' . PHP_EOL;
            }
        }        
    }

}
